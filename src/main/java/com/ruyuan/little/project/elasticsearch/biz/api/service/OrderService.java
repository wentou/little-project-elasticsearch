package com.ruyuan.little.project.elasticsearch.biz.api.service;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.elasticsearch.biz.api.dto.OrderSearchDTO;
import com.ruyuan.little.project.elasticsearch.biz.api.entity.Order;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
public interface OrderService {

    /**
     * 创建订单
     * @param order 订单信息
     * @return 结果
     */
    CommonResponse createOrder(Order order);


    /**
     * 支付订单
     * @param order 订单信息
     * @return 结果
     */
    CommonResponse pay(Order order);


    /**
     * 取消订单
     * @param order 订单信息
     * @return 结果
     */
    CommonResponse cancel(Order order);

    /**
     * 根据id获取订单详情
     * @param id 订单id
     * @return 结果
     */
    CommonResponse getOrderById(String id);


    /**
     * 确认收货
     * @param order 订单信息
     * @return 结果
     */
    CommonResponse receive(Order order);


    /**
     * 发布评论信息
     * @param order 订单信息
     * @return 结果
     */
    CommonResponse publishComment(Order order);

    /**
     * 查询订单列表
     *
     * @param orderSearchDTO    订单查询请求信息
     * @return                  结果
     */
    CommonResponse getOrderList(OrderSearchDTO orderSearchDTO);

}
