package com.ruyuan.little.project.elasticsearch.biz.api.dto;

import com.ruyuan.little.project.elasticsearch.biz.common.enums.QueryTypeEnum;
import lombok.Data;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
@Data
public class GoodsSearchDTO {

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 起始金额范围
     */
    private Double startPrice;

    /**
     * 结束金额范围
     */
    private Double endPrice;

    /**
     * 排序字段
     */
    private String orderField;

    /**
     * 排序类型 asc desc
     */
    private String orderType;

    /**
     * 页码
     */
    private Integer page;

    /**
     * 每页大小
     */
    private Integer size;

    /**
     * 查询类型
     * {@link QueryTypeEnum}
     */
    private Integer queryType;
}
